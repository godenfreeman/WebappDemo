/**
 * Created by Administrator on 2017/3/13.
 */
"use strict";
 angular.module('app').filter('filterByObj',[function () {
     return function (list,obj) {
         var result = [];
         angular.forEach(list,function (item) {
             var isEqual = true;
             for(var i in obj){
                 if(item[i] !==obj[i]){
                     isEqual = false
                 }
             }
             if (isEqual){
                 result.push(item)
             }
         })
         return result;
     }
 }])