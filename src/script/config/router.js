/**
 * Created by Administrator on 2017/2/14.
 */
"use strict";
angular.module("app", ['ui.router']).config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("main");
    $stateProvider.state("main", {
        url: "/main",
        templateUrl: "view/main.html",
        controller: "mainCtrl"
    })
        .state("position", {
            url: "/position/:id",
            templateUrl: "view/position.html",
            controller: "positionCtrl"
        })
        .state("companyDec", {
            url: "/companyDec/:id",
            templateUrl: "view/companyDec.html",
            controller: "companyCtrl"
        })
        .state("search", {
            url: "/search/",
            templateUrl: "view/search.html",
            controller: "searchCtrl"
        })
        .state("login", {
            url: "/login",
            templateUrl: "view/login.html",
            controller: "loginCtrl"
        })
        .state("register", {
            url: "/register",
            templateUrl: "view/register.html",
            controller: "registerCtrl"
        })
        .state("favorite", {
            url: "/favorite",
            templateUrl: "view/favorite.html",
            controller: "favoriteCtrl"
        })
        .state("person", {
            url: "/person",
            templateUrl: "view/person.html",
            controller: "personCtrl"
        })
        .state("postList", {
            url: "/postList",
            templateUrl: "view/postList.html",
            controller: "postListCtrl"
        })

}]);