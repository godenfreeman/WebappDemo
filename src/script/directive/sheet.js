/**
 * Created by Administrator on 2017/3/5.
 */
'use strict';
angular.module('app').directive('appSheet',[function () {
    return {
        restrict:'A',
        replace:true,
        scope:{
            list:"=",
            visible:'=',
            select:'&'
        },
        templateUrl:'/view/template/sheet.html',
        link:function (scope) {

        }
    }
}]);