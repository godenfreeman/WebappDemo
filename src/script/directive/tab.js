/**
 * Created by Administrator on 2017/3/5.
 */
'use strict';
angular.module('app').directive('appTab',[function () {
    return {
        restrict:'A',
        replace:true,
        scope:{
            list:'=',
            tabclick:'&',
        },
        templateUrl:'/view/template/tab.html',
        link:function (scope) {
            scope.click = function (tab) {
                scope.selectid = tab.id;
                scope.tabclick(tab);
            }
        }
    }
}]);