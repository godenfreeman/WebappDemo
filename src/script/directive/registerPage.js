/**
 * Created by Administrator on 2017/3/29.
 */
"use strict";
angular.module("app").directive("appRegisterPage",['$state',function ($state) {
    return {
        restrict:"A",
        replace:true,
        templateUrl:"view/template/registerPage.html",
        link:function (scope) {
            scope.back = function () {
                $state.go('main')
            }
        }
    }
}]);