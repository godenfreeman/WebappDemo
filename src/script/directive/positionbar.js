/**
 * Created by Administrator on 2017/2/21.
 */
"use strict";
angular.module("app").directive("appHeadBar",[function () {
    return {
        restrict:"A",
        replace:true,
        scope:{
            title:'@'
        },
        templateUrl:"view/template/headbar.html",
        link:function (scope) {
            scope.back = function () {
                window.history.back();
            }

        }
    }
}]);