/**
 * Created by Administrator on 2017/2/19.
 */
"use strict";
angular.module("app").directive("appFooter",[function () {
    return {
        restrict:"A",
        replace:true,
        templateUrl:"view/template/footer.html"
    }
}]);
