/**
 * Created by Administrator on 2017/3/5.
 */
'use strict';
angular.module('app').directive('appPositionClass',[function () {
    return {
        restrict:'A',
        replace:true,
        scope:{
            com:'='
        },
        templateUrl:'/view/template/positionclass.html',
        link:function (scope) {
            scope.showPositionlist = function (idx) {
                scope.positionList = scope.com.positionClass[idx].positionList;
                console.log(scope.positionList)
                scope.isActive = idx;
            };
            scope.$watch('com',function (newVal) {
                if (newVal){
                    scope.showPositionlist(0);
                }
            });
            scope.$on('abc',function (event,data) {
                console.log(event,data)
            });
            scope.$emit('cba',{name:"king"});

        }
    }
}]);