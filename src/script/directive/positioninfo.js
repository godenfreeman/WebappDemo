/**
 * Created by Administrator on 2017/3/5.
 */
'use strict';
angular.module('app').directive('appPositionInfo',[function () {
    return {
        restrict:'A',
        replace:true,
        scope:{
            isLogin:'=',
            pos:'='
        },
        templateUrl:'/view/template/positionInfor.html',
        link:function ($scope) {
            $scope.$watch("pos",function (newVal) {
                if(newVal){
                    $scope.pos.select = $scope.pos.select || false;
                    $scope.imgPath = $scope.pos.select? '/images/star-active.png':'/images/star.png';
                }
            });
            $scope.favorite = function () {
                $scope.pos.select = !$scope.pos.select;
                $scope.imgPath = $scope.pos.select? '/images/star-active.png':'/images/star.png'
            }
        }
    }
}]);