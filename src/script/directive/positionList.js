/**
 * Created by Administrator on 2017/2/21.
 */
'use strict';
angular.module('app').directive('appPositionList',["$http",function ($http) {
    return {
        restrict:'A',
        replace:true,
        scope:{
             data:'=',
             filterObj:'=',
             isFavorite:'='
        },
        templateUrl:'/view/template/positionList.html',
        link:function ($scope) {
            $scope.name = sessionStorage.getItem("name")||"";
            $scope.select = function (item) {
                item.select = !item.select;
                // item.select = sessionStorage.setItem("select",!item.select);

                // $http({
                //     method: 'POST',
                //     url: '/data/favorite.json',
                // }).then(function successCallback(res) {
                //     item.select = !item.select
                // }, function errorCallback(res) {
                //     console.log("error...")
                // });
            }
        }
    }
}]);