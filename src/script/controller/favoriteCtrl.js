/**
 * Created by Administrator on 2017/4/9.
 */
"use strict";
angular.module('app').controller('favoriteCtrl',['$scope','$http',function ($scope,$http) {
    $http({
        method: 'GET',
        url: '/data/myFavorite.json'
    }).then(function successCallback(res) {
        $scope.list = res.data
    }, function errorCallback(res) {
        $log("error...")
    });
}]);