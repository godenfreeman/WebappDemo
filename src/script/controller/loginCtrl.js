/**
 * Created by Administrator on 2017/3/29.
 */
"use strict";
angular.module('app').controller('loginCtrl',['$scope','$http','$state','$log',function ($scope,$http,$state,$log) {
    $scope.login = function () {
        var data = {
            phone:sessionStorage.getItem("phone"),
            password:sessionStorage.getItem("password")
        };
        $http({
            method: 'GET',
            url: '/data/login.json'
        }).then(function successCallback(res) {
            if($scope.user.phone == data.phone && $scope.user.password == data.password){
                sessionStorage.setItem("id",res.data.id);
                sessionStorage.setItem("name",res.data.name);
                sessionStorage.setItem("img",res.data.image);
                $state.go("main")
            } else {
                alert("您输入的用户不存在")
            }
        },function errorCallback(res) {
            $log("error...")
        });




    }
}]);