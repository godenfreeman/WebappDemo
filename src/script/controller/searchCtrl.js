/**
 * Created by Administrator on 2017/2/21.
 */
angular.module('app').controller('searchCtrl',['$scope','$http','dict',function ($scope,$http,dict) {
    $scope.name = '';
    $scope.search = function () {
        $http({
            method: 'GET',
            url: '/data/positionList.json?name='+ $scope.name
        }).then(function successCallback(response) {
            $scope.poslist = response.data
        }, function errorCallback(response) {
            console.log('error...')
        });
    }
    $scope.filterObj = {};
    $scope.search();

    $scope.sheet ={};
    $scope.tablist = [
        {
            id:"city",
            name:'城市'
        },
        {
            id:"salary",
            name:'薪水'
        },
        {
            id:"scale",
            name:'公司规模'
        }
    ];
    var tabid = '';
    $scope.tClick = function (id,name) {
        tabid = id;
        console.log(id,name);
        $scope.sheet.list = dict[id];
        $scope.visible = true;
    };

    $scope.sClick = function (id,name) {
        console.log(id,name);
        if(id){
            angular.forEach($scope.tablist,function (item) {
                if(item.id===tabid){
                    item.name = name;
                }
            })
            $scope.filterObj[tabid + 'Id'] = id
        }else {
            delete $scope.filterObj[tabid + 'Id'] 
            angular.forEach($scope.tablist,function (item) {
                if(item.id===tabid){
                    switch (item.id){
                        case 'city':
                            item.name = '城市';
                        break;
                        case 'salary':
                            item.name = '薪水';
                            break;
                        case 'scale':
                            item.name = '公司规模';
                            break;
                    }
                }
            })
        }
    }

}]);