/**
 * Created by Administrator on 2017/2/21.
 */
"use strict";
angular.module('app').controller('companyCtrl',['$scope','$http','$state',function ($scope,$http,$state) {
    $http({
        method: 'GET',
        url: '/data/company.json?id='+$state.params.id
    }).then(function successCallback(response) {
        $scope.company = response.data;
        // $scope.$broadcast('abc',{id:1});
    }, function errorCallback(response) {
        console.log('error...')
    });
    // $scope.$on('cba',function (event,data) {
    //     console.log(event,data)
    // })
}]);