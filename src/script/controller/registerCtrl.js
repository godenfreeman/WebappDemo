/**
 * Created by Administrator on 2017/3/29.
 */
"use strict";
angular.module('app').controller('registerCtrl', [ '$interval','$scope','$http','$state', function ($interval,$scope,$http,$state) {
    //提交注册
    $scope.sumbit = function () {
        // console.log($scope.user);
        var data = {
            phone:$scope.user.phone,
            password:$scope.user.password
        };
        console.log(data)
        sessionStorage.setItem("phone",data.phone);
        sessionStorage.setItem("password",data.password);
        $state.go("login")
    };

    var count = 60;
    //发送验证码
    $scope.send = function () {
        $http({
            method: 'GET',
            url: '/data/code.json'
        }).then(function successCallback(rsp) {
            if (1 === rsp.data.state ) {
                $scope.time = '60s';
                var interval = $interval(function () {
                    if(count <= 0){
                        count = 60;
                        $interval.cancel(interval);
                        $scope.time = '';
                    } else {
                        count--;
                        $scope.time = count-- + 's';
                    }
                }, 1000)
            }
            changeAuthCode();
        }, function errorCallback(response) {
            console.log('error...')
        });
    }
    function changeAuthCode() {
        if($scope.user.authCode ==''){
            $scope.user.authCode=123456;
        }
    }
}]);