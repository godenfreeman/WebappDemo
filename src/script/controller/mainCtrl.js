/**
 * Created by Administrator on 2017/2/16.
 */
"use strict";
angular.module("app").controller("mainCtrl", ["$scope", "$http", function ($scope, $http) {
    $http({
        method: 'GET',
        url: '/data/positionList.json'
    }).then(function successCallback(response) {
        console.log(response);
        $scope.list = response.data
    }, function errorCallback(response) {
        console.log('error...')
    });

}]);