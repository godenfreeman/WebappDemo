/**
 * Created by Administrator on 2017/2/21.
 */
"use strict";
angular.module('app').controller('positionCtrl',['$scope','$http','$state','$q','$log',function ($scope,$http,$state,$q) {
    $scope.isLogin = sessionStorage.getItem("name");
    $scope.message = $scope.isLogin?'投个简历':'去登录';
    function getPositon() {
        var def = $q.defer();
        $http({
            method: 'GET',
            url: '/data/position.json?id='+$state.params.id
        }).then(function successCallback(response) {
            $scope.position = response.data;
            //判断是否已经用户投递
            if(response.data.posted){
                $scope.message = "已投递"
            }
            def.resolve(response.data);
        }, function errorCallback(response) {
            def.reject(response)
        });
        return def.promise;
    }
    function getCompanyid(id) {
        $http({
            method: 'GET',
            url: '/data/company.json?id='+id
        }).then(function successCallback(response) {
            $scope.company = response.data;
        }, function errorCallback(response) {
           console.log('error...')
        });
    }
    getPositon().then(function (obj) {
        getCompanyid(obj.companyId)
    });
    //修改投递状态
    $scope.go = function () {
        if($scope.message !=="已投递"){
            if($scope.isLogin){
                $scope.message ="已投递"
            } else {
                $state.go("login")
            }
        }
    }
}]);