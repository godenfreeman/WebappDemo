/**
 * Created by Administrator on 2017/4/9.
 */
"use strict";
angular.module("app").controller("personCtrl", ["$scope","$http","$state", function ($scope, $http,$state) {
    if(sessionStorage.getItem("phone")){
        $scope.name = sessionStorage.getItem("name")
        $scope.image = sessionStorage.getItem("img")
    }
    $scope.logout = function () {
        sessionStorage.clear()
        $state.go("login")
    }
}])