/**
 * Created by Administrator on 2017/4/9.
 */
"use strict";
angular.module("app").controller("postListCtrl", ["$scope","$http","$log", function ($scope,$http,$log) {
    $scope.tabList = [
        {
            id: 'all',
            name: '全部'
        },
        {
            id: 'pass',
            name: '邀请面试'
        },
        {
            id: 'fail',
            name: '不合适'
        }
    ]
    $http({
        method: 'GET',
        url: '/data/myPost.json'
    }).then(function successCallback(res) {
        $scope.post = res.data
    }, function errorCallback(res) {
        $log("error...")
    });
    $scope.filterObj = {};
    $scope.tClick = function (id,name) {
        switch (id){
            case "all":
                delete $scope.filterObj.state;
                break;
            case "pass":
                $scope.filterObj.state = "1";
                break;
            case "fail":
                $scope.filterObj.state = "-1";
                break;
            default:
        }
    }

}]);