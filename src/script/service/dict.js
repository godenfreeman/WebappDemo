/**
 * Created by Administrator on 2017/3/13.
 */
"use strict";
angular.module('app').value('dict',{}).run(['dict','$http',function (dict,$http) {
    $http({
        method: 'GET',
        url: '/data/city.json'
    }).then(function successCallback(response) {
        dict.city = response.data
    }, function errorCallback(response) {
        console.log('error...')
    });
    $http({
        method: 'GET',
        url: '/data/salary.json'
    }).then(function successCallback(response) {
        dict.salary = response.data
    }, function errorCallback(response) {
        console.log('error...')
    });
    $http({
        method: 'GET',
        url: '/data/scale.json'
    }).then(function successCallback(response) {
        dict.scale = response.data
    }, function errorCallback(response) {
        console.log('error...')
    });
}])